# -*- coding: utf-8 -*-
###############################################################################
#       seafform/management/set_admin.py
#       
#       Copyright © 2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of seafform.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafform management: set an existing user as admin"""

__author__ = "Florence Birée"
__version__ = "0.7.1"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023, Florence Birée <florence@biree.name>"

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

class Command(BaseCommand):
    help = "Set an existing user as admin (use it's Seafile `email` ID)"

    def add_arguments(self, parser):
        parser.add_argument("email")

    def handle(self, *args, **options):
        try:
            user = User.objects.get(username=options["email"])
        except User.DoesNotExist:
            raise CommandError('User {} does not exist'.format(options["email"]))
        
        user.is_staff = True
        user.is_superuser = True
        user.save()
        
        self.stdout.write(
            self.style.SUCCESS('Successfully updated user {}'.format(options["email"]))
        )

