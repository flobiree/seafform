# -*- coding: utf-8 -*-
###############################################################################
#       seafform/forms.py
#       
#       Copyright © 2020-2023, Florence Birée <flo@biree.name>
#       
#       This file is a part of seafform.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafform Django forms description"""

__author__ = "Florence Birée"
__version__ = "0.7.1"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023, Florence Birée <flo@biree.name>"

from uuid import uuid4
from django import forms
from django.forms.widgets import SelectDateWidget
from django.utils.translation import gettext as _
import seafform.seafform as seafform
from django.conf import settings

class LoginForm(forms.Form):
    email = forms.CharField(label=(
        getattr(settings, 'LOGIN_LABEL', _("Username or e-mail"))
    ))
    password = forms.CharField(widget=forms.PasswordInput, label=_("Password"))
    # autofocus to email
    email.widget.attrs.update({'autofocus' : 'autofocus'})

class DjForm(forms.Form):
    
    required_css_class = 'required'
    use_required_attribute = False

    rowid = forms.CharField(widget=forms.HiddenInput, initial='newrow')
    rowid.isstatic = False
    
    def __init__(self, *args, **kwargs):
        """Add one more arguments:
            fieldlist: a seafform.Fields list
        """
        fieldlist = kwargs.pop('fieldlist')
        form_url = kwargs.pop('form_url')
        super(DjForm, self).__init__(*args, **kwargs)
        
        def unique_edit_url():
            return (form_url + 'edit/' + str(uuid4()).replace('/', 'x') + '/')
        
        firstfield = None
        
        for field in fieldlist:
            stdparams = {
                'label': field.label,
                'required': field.required,
                'help_text': field.description,
            } 
            djfield = None
            if isinstance(field, seafform.TextField):
                djfield = forms.CharField(**stdparams)
            elif isinstance(field, seafform.LongTextField):
                params = stdparams.copy()
                params.update(widget = forms.Textarea)
                djfield = forms.CharField(**params)
            elif isinstance(field, seafform.ListField):
                params = stdparams.copy()
                params.update(choices = ((c, c) for c in field.choices))
                djfield = forms.ChoiceField(**params)
            elif isinstance(field, seafform.BooleanField):
                params = stdparams.copy()
                params.update(initial = False)
                djfield = forms.BooleanField(**params)
            elif isinstance(field, seafform.BooleanTrueField):
                params = stdparams.copy()
                params.update(initial = True)
                djfield = forms.BooleanField(**params)
            elif isinstance(field, seafform.DateField):
                params = stdparams.copy()
                params.update(widget = SelectDateWidget)
                djfield = forms.DateField(**params)
                djfield.widget.attrs.update(
                    {'style': 'width: 32%; display: inline-block;'}
                )
            elif isinstance(field, seafform.NumberField):
                djfield = forms.FloatField(**stdparams)
            elif isinstance(field, seafform.StaticField):
                params = stdparams.copy()
                params.update(widget=forms.HiddenInput)
                params['required'] = False
                djfield = forms.CharField(**params)
                djfield.isstatic = True
            elif isinstance(field, seafform.EditLinkField):
                params = stdparams.copy()
                if not field.description:
                    # add an std label
                    params.update(help_text=_("Save this link to come back and edit"))
                #params.update(widget=forms.HiddenInput)
                params.update(initial = unique_edit_url())
                #params.update(disabled = True)
                djfield = forms.URLField(**params)
                djfield.isstatic = True
                djfield.widget.attrs.update(
                    {'readonly': True}
                )
            elif isinstance(field, seafform.CancelField):
                # field added only if the form is bound
                if self.is_bound and 'rowid' in self.changed_data:
                    params = stdparams.copy()
                    params.update(initial = False)
                    if not field.label:
                        # add an std label 
                        params.update(label=_("Cancel"))
                
                    if not field.description:
                        # add an std label
                        params.update(help_text=_("Check this box to cancel your answer"))
                    djfield = forms.BooleanField(**params)
                    djfield.widget.attrs.update(                                     
                        {'style': 'color: red;'}              
                    )
            elif isinstance(field, seafform.UploadField):
                params = stdparams.copy()
                params.update(widget = forms.ClearableFileInput)
                if self.is_bound:
                    params['required'] = False
                djfield = forms.FileField(**params)
            
            if djfield is not None:
                #djfield.widget.attrs = {'id': field.f_id}
                self.fields[field.f_id] = djfield
                if not hasattr(djfield, 'isstatic'):
                    djfield.isstatic = False
            
            if firstfield is None:
                firstfield = self.fields[field.f_id]
        
        # add autofocus to the first one
        firstfield.widget.attrs.update({'autofocus' : 'autofocus'})
        
        
