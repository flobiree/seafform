# -*- coding: utf-8 -*-
###############################################################################
#       seafform/seafform.py
#       
#       Copyright © 2020-2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of seafform.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafform forms description"""

__author__ = "Florence Birée"
__version__ = "0.7.1"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023, Florence Birée <florence@biree.name>"

import os
import ezodf
import shutil
import time
import datetime
from random import randrange
from tempfile import NamedTemporaryFile
from string import ascii_uppercase
from seafform.seafile_webapi import NotFound
from django.core.files.uploadedfile import SimpleUploadedFile
if 'DJANGO_SETTINGS_MODULE' in os.environ:
    from django.utils.translation import gettext_noop as ugettext_noop 
    from django.utils.translation import gettext as _
else:
    # Not in a Django environment
    _ = lambda x:x
    ugettext_noop = _

HEADERS_ROW = 4  # number of headers row in ods files
#all_less_maxcount strategy delete formats
# trying all_but_last, then all
#ezodf.config.set_table_expand_strategy('all_but_last')

def col_idx(num):
    """Return the name of the column from its index"""
    if num < len(ascii_uppercase):
        return ascii_uppercase[num]
    else:
        return alpha(num // len(ascii_uppercase) - 1) + ascii_uppercase[num % len(ascii_uppercase)]

def col_reverse_idx(name):
    """Return the numeric index of the column from its sheet name"""
    index = 0
    for i, letter in enumerate(reversed(name)):
        index += len(ascii_uppercase)**i * (ascii_uppercase.find(letter) + 1)
    return index - 1

class InvalidODS(Exception):
    """Raise when the ODS file doesn't respect the specification for Seafform
    """
    def __init__(self, msg):
        self.msg = msg

class Field:
    """Base class for form fields"""
    
    def __init__(self, label, f_id, description=None, params=None, required=False, 
                       value=None):
        """Initialize a new field"""
        self.label = label
        self.description = description
        self.params = params
        self.required = required
        self.value = value
        self.f_id = f_id
    
    def __repr__(self):
        if hasattr(self, 'ident'):
            ftype = self.ident
        else:
            ftype = 'Field'
        return '<{0}({1}){2}>'.format(
            ftype,
            self.label,
            ('*' if self.required else '')
        )

class TextField(Field):
    """Single-line text field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('text')

class LongTextField(Field):
    """Multiline text field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('longtext')

class ListField(Field):
    """List of choices field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('list')
    
    def __init__(self, *args):
        Field.__init__(self, *args)
        self.choices = [
            ch.strip() for ch in self.params.split(',')
        ]

class BooleanField(Field):
    """Checkbox field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('check')

    def __init__(self, *args):
        Field.__init__(self, *args)
        self.value = False

class BooleanTrueField(Field):
    """Checked checkbox field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('checked')
    
    def __init__(self, *args):
        Field.__init__(self, *args)
        self.value = True

class DateField(Field):
    """Date field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('date')

class NumberField(Field):
    """Number field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('number')

class StaticField(Field):
    """Non-editable field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('static')

class EditLinkField(Field):
    """Edit-link field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('editlink')

class CancelField(Field):
    """Cancel box field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('cancel')

class UploadField(Field):
    """Upload file field"""
    # Translators: field type for spreadsheet
    ident = ugettext_noop('upload')

    def __init__(self, *args):
        Field.__init__(self, *args)
        if not self.params or not ',' in self.params:
            raise InvalidODS(_("Field \"{}\": the parameter must contain something like 'subdir,B+C'").format(self.f_id))
        self.subdir, self.colnames = [s.strip() for s in self.params.split(',')]
        if not self.subdir:
            raise InvalidODS(_("Field \"{}\": you must set a subdir in the parameter (like 'subdir,B+C')").format(self.f_id))
        if not self.colnames:
            raise InvalidODS(_("Field \"{}\": you must set a colomun name in the parameter (like 'subdir,B')").format(self.f_id))
        self.colnames = self.colnames.upper().split('+')

def field_of(ident):
    """Return the Field subclass corresponding to `ident`"""
    return [
        cls 
        for cls in Field.__subclasses__() 
        if (cls.ident == ident) or (_(cls.ident) == ident)
    ][0]

def untranslate(loc_val, raw_list, loc_list):
    """If loc_val in raw_list,
        return loc_val
    else:
        Return the raw value at the same position in raw_list than
        loc_val in loc_list
    default to the first value
    """
    if loc_val in raw_list:
        return loc_val
    else:
        try:
            return raw_list[loc_list.index(loc_val)]
        except ValueError:
            return raw_list[0]

class SeafForm:
    """Build and fill a form from an OpenDocumentSpreadsheet file"""
    _availables_f_ident = (
        [cls.ident for cls in Field.__subclasses__()]
        +
        [_(cls.ident) for cls in Field.__subclasses__()]
    )

    TITLE_CELL = 'A7'
    DESC_CELL = 'A9'    

    def __init__(self, filepath, seaf=None, repo_id=None):
        """Initialize a form for the file `filepath`.

            If seaf is a Seafile instance, repo_id must be the
            Seafile identifier of the repository where `filepath` is.

            If seaf is None, load `filepath` from the local filesystem
        """
        # source properties
        self.filepath = filepath
        self.seaf = seaf
        self.repo_id = repo_id
        self.loaded = False

        # form properties
        self.title = None
        self.description = None
        self.fields = None
        self.data = None
        # Translators: ODS view mode
        self._view_as_values = (ugettext_noop('table'), ugettext_noop('form'))
        self._view_as_l10n = [_(v) for v in self._view_as_values]
        self.view_as = None # ('table' or 'form')
        # Translators: ODS edit, yes or no
        self._edit_values = (ugettext_noop('yes'), ugettext_noop('no'), ugettext_noop('yes + delete'))
        self._edit_val10n = [_(v) for v in self._edit_values]
        self.edit = None
        self.delete = None
        # Translators: ODS public, yes or no
        self._public_values = (ugettext_noop('yes'), ugettext_noop('no'))
        self._public_val10n = [_(v) for v in self._public_values]
        self.public = None
        
        self.limit = None # max number of answers
        
        # cached items
        self.mtime = None
        self._odsfile = None
        self._first_empty_row = None
        
        # special fields (column ids)
        self.cancel_cols = []
        
        # internals
        self.existing_f_id = []

    def __repr__(self):
        """Representation of the form"""
        if self.loaded:
            return "<SeafForm({0}:{1})>".format(self.filepath, self.title)
        else:
            return "<SeafForm({0}:unloaded)>".format(self.filepath)

    def load(self):
        """Load form data from the ODS file"""
        odsopener = self._seaf_open if self.seaf else self._local_open
        
        seaf_f = odsopener()
        # save spreadsheet into a temporary file
        with NamedTemporaryFile(delete=False) as tmpfile:
            shutil.copyfileobj(seaf_f, tmpfile)
            tmpname = tmpfile.name
        seaf_f.close()
        
        # open spreadsheet document
        self.odsfile = ezodf.opendoc(tmpname)
        # delete tmp file
        os.unlink(tmpname)
        
        
        # get the Data sheet
        try:
            datash = self.odsfile.sheets['Data']
        except KeyError:
            raise InvalidODS(_("The form file miss the 'Data' tab. Is it really built from a model?")) 
        
        # get Properties
        self.title = datash[self.TITLE_CELL].value
        if not self.title:
            raise InvalidODS(
                    _("The title cell is empty ({0})").format(self.TITLE_CELL))
        self.description = datash[self.DESC_CELL].value
        if not self.description:
            raise InvalidODS(
                _("The description cell is empty ({0})").format(self.DESC_CELL))
        self.view_as = untranslate(
            datash['A11'].value,
            self._view_as_values,
            self._view_as_l10n
        )
        
        edit_value = untranslate(
            datash['A13'].value,                                                 
            self._edit_values,                                                   
            self._edit_val10n
        )
        if edit_value == 'yes':
            self.edit = True
            self.delete = False
        elif edit_value == 'yes + delete':
            self.edit = True
            self.delete = True
        else:
            self.edit = False
            self.delete = False
        
        self.public = ('yes' == (untranslate(
            datash['A15'].value,
            self._public_values,
            self._public_val10n
        )))
        
        self.cancel_cols = []
        
        try:
            self.limit = datash['A17'].value
        except IndexError:
            self.limit = None
        else:
            if not self.limit:
                self.limit = None
            else:
                self.limit = int(self.limit)
        
        # get fields
        self.fields = []
        for colid in range(1, datash.ncols()):
            # get column
            col = datash.column(colid)
            # get field data
            fname   = col[0].value
            fformat = col[1].value
            fparams = col[2].value
            fdesc   = col[3].value
            # col[4:] is data
            try:
                fformat = fformat.lower()
            except AttributeError:
                pass
            
            if fformat is not None and fname is None:
                raise InvalidODS(
                    _("The field in {0} need a name.").format(col_idx(colid)+'1')
                )
            
            # build field object (if fformat is known)
            if fformat and fformat.strip('*') in self._availables_f_ident:
                frequired = (fformat.endswith('*'))
                FType = field_of(fformat.rstrip('*'))
                # find an unique f_id for the field
                f_id = fname.strip()
                count = 1
                while f_id in self.existing_f_id:
                    f_id += str(count)
                self.existing_f_id.append(f_id)
                if count > 1:
                    fname += ' ({0})'.format(count)
                try:
                    self.fields.append(FType(
                        fname, f_id, fdesc, fparams, frequired
                    ))
                except AttributeError:
                    raise InvalidODS(
                        _("Missing parameter for the list at {0}").format(col_idx(colid)+'3')
                    )
                if FType is CancelField:
                    self.cancel_cols.append(colid)
        
        # get data
        self.data = []
        first_empy_row = self._get_first_empty_row(recompute=True)
        for rowid in range(HEADERS_ROW, first_empy_row):
            row = datash.row(rowid)
            row_data = []
            for celid in range(1, len(self.fields) + 1):
                val = row[celid].value
                if self.fields[celid-1].ident == 'date' and val:
                    try:
                        s_time = time.strptime(val, '%Y-%m-%d')
                    except ValueError:
                        pass # keep val as str
                    else:
                        val = datetime.date(*s_time[:3])
                elif self.fields[celid-1].ident.startswith('check') and val:
                    val = bool(val)
                row_data.append(val)
            self.data.append(row_data)
        
        # get mtime
        if self.seaf:
            s = self.seaf.stat_file(self.repo_id, self.filepath)
            self.mtime = float(s['mtime'])
        else:
            self.mtime = os.path.getmtime(self.filepath)
        
        self.loaded = True
    
    def _seaf_open(self):
        """Return an opened file-like object from Seafile"""
        return self.seaf.open_file(self.repo_id, self.filepath)

    def _local_open(self):
        """Return an opened file object from local filesystem"""
        return open(self.filepath, 'rb')
    
    def _get_first_empty_row(self, recompute=False):
        """Return the first empty row number
            if recompute, do not use the cached value
        """
        if self._first_empty_row is not None and not recompute:
            return self._first_empty_row
        
        # get the Data sheet
        try:
            datash = self.odsfile.sheets['Data']
        except KeyError:
            raise InvalidODS 
                
        # find the first empty row
        rowid = datash.nrows() - 1
        bcol = datash.column(1)
        for celid in reversed(range(HEADERS_ROW, datash.nrows())):
            if not bcol[celid].value:
                rowid = celid
            else:
                break
        for colid in range(1, datash.ncols()):
            # check if rowid is empty for all the row
            # go down until empty
            while (rowid < datash.nrows() and datash[rowid, colid].value):
                rowid += 1
        
        self._first_empty_row = rowid 
        return rowid
    
    def post(self, values, replace_row=None):
        """Post data from values into the ODS file

            values is the dict of {ident: value}
            optionaly replace values from the row `replace_row`
            
            WARNING: type and required verification must be done before
        """
        # get new mtime
        if self.seaf:
            s = self.seaf.stat_file(self.repo_id, self.filepath)
            new_mtime = float(s['mtime'])
        else:
            new_mtime = os.path.getmtime(self.filepath)
        # if mtime has changed:
        if self.mtime != new_mtime:
            self.load() # reload
        
        # get the Data sheet
        try:
            datash = self.odsfile.sheets['Data']
        except KeyError:
            raise InvalidODS 
        
        
        # save data in a new line        
        if replace_row is None:
            rowid = self._get_first_empty_row()
            self.data.append([])
            self._first_empty_row += 1
        else:
            rowid = replace_row
        
        # fill the row with values
        for colid in range(1, datash.ncols()):
            column = datash.column(colid)
            try:
                field = self.fields[colid - 1]
            except IndexError:
                break
            f_id = field.f_id
            if f_id in values and values[f_id] is not None:
                if values[f_id] is True:
                    value = 1
                elif values[f_id] is False:
                    value = ""
                else:
                    value = values[f_id]
                
                try:
                    column[rowid].set_value(value)
                except IndexError:
                    # add row
                    datash.append_rows(1)
                    column = datash.column(colid)
                    column[rowid].set_value(value)
                
                # convert to boolean for cached data
                if self.fields[colid-1].ident.startswith('check') and value:
                    value = bool(value)
                try:
                    self.data[rowid - HEADERS_ROW][colid - 1] = value
                except IndexError:
                    self.data[rowid - HEADERS_ROW].append(value)
            else:
                try:
                    self.data[rowid - HEADERS_ROW][colid - 1] = None
                except IndexError:
                    self.data[rowid - HEADERS_ROW].append(None)
        
        if self.seaf:        
            # save spreadsheet into a temporary file
            with NamedTemporaryFile(delete=False) as tmpfile:
                # ezodf realy doesn't like file-like objects…
                tmpname = tmpfile.name
            self.odsfile.saveas(tmpname)
                
            # update distant file
            with open(tmpname, 'rb') as fileo:
                fid = self.seaf.update_file(self.repo_id, self.filepath, fileo)
            # unlink tmp file
            os.unlink(tmpname)
        else:
            # save spreadsheet into the local file
            self.odsfile.saveas(self.filepath)

    def get_values_from_data(self, row_id):
        """Build the dict of {'name': value} for row_id from self.data"""
        vals = {}
        for i, field in enumerate(self.fields):
            vals[field.f_id] = self.data[row_id - HEADERS_ROW][i]
        return vals
    
    def get_files_from_data(self, row_id):
        """Build the dict of {'name': UploadedFile} for row_id from self.data"""
        vals = {}
        for i, field in enumerate(self.fields):
            if isinstance(field, UploadField): 
                try:
                    filename = self.data[row_id - HEADERS_ROW][i]
                    if not filename:
                        continue
                    basedir = os.path.dirname(self.filepath)
                    filepath = os.path.join(basedir, field.subdir)
                    # get the file content
                    if self.seaf:
                        file_o = self.seaf.open_file(self.repo_id, os.path.join(filepath, filename))
                        url = self.seaf.get_or_create_share_link(self.repo_id, os.path.join(filepath, filename))
                    else:
                        file_o = open(os.path.join(filepath, filename), 'rb')
                        url = "file://" + os.path.join(filepath, filename) 
                    vals[field.f_id] = SimpleUploadedFile(filename, file_o.read())
                    vals[field.f_id].url = url
                    file_o.close()
                except FileNotFoundError:
                    pass
        return vals
    
    def get_values_from_uuid_url(self, uuid_url):
        """Return (initials, files) values from uuid_url"""
        # check if there is a editlink field and its position
        editfieldpos = None
        for i in range(len(self.fields)):
            if self.fields[i].ident == 'editlink':
                editfieldpos = i
                break
        if editfieldpos is None:
            raise KeyError
        
        rowid = None
        for i, row in enumerate(self.data):
            if row[editfieldpos] == uuid_url:
                rowid = i + HEADERS_ROW
        if rowid is None:
            raise KeyError

        initials = self.get_values_from_data(rowid)
        initials['rowid'] = rowid
        files = self.get_files_from_data(rowid)
        return (initials, files)

    def delete_row(self, delete_row):
        """Delete data from the row `delete_row`.
        
            If there is data below, put the data up a row
        """
        # get new mtime
        if self.seaf:
            s = self.seaf.stat_file(self.repo_id, self.filepath)
            new_mtime = float(s['mtime'])
        else:
            new_mtime = os.path.getmtime(self.filepath)
        # if mtime has changed:
        if self.mtime != new_mtime:
            self.load() # reload
        
        # get the Data sheet
        try:
            datash = self.odsfile.sheets['Data']
        except KeyError:
            raise InvalidODS 
        
        # if delete row is not the last row, put all data following to one row up
        if delete_row < self._get_first_empty_row(True) - 1:
            for rowid in range(delete_row, self._get_first_empty_row() - 1):
                for colid in range(1, datash.ncols()):
                    column = datash.column(colid)
                    column[rowid].set_value(
                        column[rowid + 1].value
                        if column[rowid + 1].value is not None
                        else ""
                    )
                    try:
                        newvalue = self.data[rowid + 1 - HEADERS_ROW][colid]
                    except:
                        newvalue = ""
                    try:
                        self.data[rowid - HEADERS_ROW][colid] = newvalue
                    except IndexError:
                        self.data[rowid - HEADERS_ROW].append(newvalue)
            # then delete the last row to avoid having twice the same answer
            delete_row = self._get_first_empty_row() - 1
        
        # if delete_row is the last row, empty it
        if delete_row == self._get_first_empty_row(True) - 1:
            for colid in range(1, datash.ncols()):
                column = datash.column(colid)
                column[delete_row].set_value("")
                try:
                    self.data[delete_row - HEADERS_ROW][colid - 1] = ""
                except IndexError:
                    pass # the cell doesn't exist
        
        # recompute the first_empy_row
        self._get_first_empty_row(True)
        
        if self.seaf:        
            # save spreadsheet into a temporary file
            with NamedTemporaryFile(delete=False) as tmpfile:
                # ezodf realy doesn't like file-like objects…
                tmpname = tmpfile.name
            self.odsfile.saveas(tmpname)
                
            # update distant file
            with open(tmpname, 'rb') as fileo:
                fid = self.seaf.update_file(self.repo_id, self.filepath, fileo)
            # unlink tmp file
            os.unlink(tmpname)
        else:
            # save spreadsheet into the local file
            self.odsfile.saveas(self.filepath)

    def upload_file(self, field, file_o, row_data, row_id=None):
        """Send to Seafile the file_o using parameters from field_name
        
            This will create the subdir to store the file if needed
            The subdir name and the filename will be build using the field
            parameters, and `row_id`.
            
            If `row_id` is None, we assume a new line will be created.
        """
        # Get infos
        basedir = os.path.dirname(self.filepath)
        subdir_path = os.path.join(basedir, field.subdir)
        # state dir && mkdir dir
        if self.seaf:
            try:
                self.seaf.list_dir(self.repo_id, subdir_path)
            except NotFound:
                try:
                    self.seaf.make_dir(self.repo_id, subdir_path)
                except:
                    raise InvalidODS(_("Failed to create the subdir {0} to put files for {1}.").format(field.subdir, field.f_id))
        else:
            # use os to check and create the dir
            if not os.path.isdir(subdir_path):
                try:
                    os.mkdir(subdir_path)
                except:
                    raise InvalidODS(_("Failed to create the subdir {0} to put files for {1}.").format(field.subdir, field.f_id))
        # make file name with parameters and row_id
        if row_id is None:                                                  
            row_id = self._get_first_empty_row()                                  
        filename = "{} - ".format(row_id)
        
        for colname in field.colnames:
            col_i = col_reverse_idx(colname)
            fieldname = self.fields[col_i - 1].label
            filename += "{} ".format(row_data[fieldname])
        
        filename += " - {}".format(file_o.name)
        file_o.name = filename
        # upload file
        if self.seaf:
            self.seaf.upload_file(self.repo_id, subdir_path, file_o)
        else:
            with open(os.path.join(subdir_path, filename), 'wb') as destination:
                for chunk in file_o.chunks():
                    destination.write(chunk)
        
