# -*- coding: utf-8 -*-
###############################################################################
#       seafform/admin.py
#       
#       Copyright © 2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of seafform.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafform admin site description"""

__author__ = "Florence Birée"
__version__ = "0.7.1"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023, Florence Birée <florence@biree.name>"

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.admin import AdminSite
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

# Register your models here.
from . import models

# Customize the form administration
class FormAdmin(admin.ModelAdmin):
    list_display = ('creation_datetime', 'owner', 'title', 'public')
    list_filter = ('owner', 'public')
    readonly_fields = ('creation_datetime',)
    search_fields = ['title']
    view_on_site = True

# Define an inline admin descriptor for SeafileUserInline model
# which acts a bit like a singleton
class SeafileUserInline(admin.StackedInline):
    model = models.SeafileUser
    can_delete = False
    verbose_name_plural = 'Seafile users'

# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (SeafileUserInline, )

# Customize the admin site
class SeafformAdminSite(AdminSite):
    site_header = _('Seafile forms administration')

admin_site = SeafformAdminSite('Seafform admin')

# Register Form
admin_site.register(models.Form, FormAdmin)

# Re-register UserAdmin
#admin_site.unregister(User)
admin_site.register(User, UserAdmin)


