# Seafile forms - installation instructions

## Requierments

- Python 3
- Python virtual environments
- Django 3.2
- Python3-lxml
- Python3-requests
- Python3-ezodf (not in Debian packages)
- Python3-bootstrapform

You really should install SeafForm under HTTPS, since personnal informations
will be transfered in the network

## Where you should install SeafForm?

SeafForm use the same credentials than the Seafile server. To avoid leaking
username and passwords of Seafile users, you should only install SeafForm either:

- on a server administrated by the same people than the Seafile one (you
  trust the same people)
- on your personnal server for you own use (if you can trust yourself)

With Seafform 0.7, if you want to use Seafile-session authentication
(compatible with Seafile SSO accounts like OAuth), you need to install
Seafform on a subdirectory of the Seafile host.

## How to install it?

### Get the code
*put it in a directory not in your server document root, lets says:*

    $ cd /var/webapps/
    $ git clone https://framagit.org/florencebiree/seafform.git
    $ cd seafform
    $ git checkout tags/v0.7.2

### Dependencies
*here are the Debian/Bullseye packages, if needed libs are not packaged in
you distribution, install them using pip at the next step*

    # apt install python3 python3-virtualenv python3-django python3-bootstrapform python3-lxml python3-requests virtualenv

### Initialize the virtual environment and dependencies

    $ # in the seafform directory
    $ virtualenv --python=python3 --system-site-packages venv/
    $ cd venv/ # go to the virtual env to install python packages
    $ . bin/activate
    $ pip install ezodf gunicorn

## Create a Seafile library for forms templates

Get the content of the _forms-library_ directory, and put it inside a Seafile library. Get a share link for this library.

## Configuring the app

In venv/seafformsite/seafformsite/ copy settings.py.sample to settings.py, and
change it for your needs (read the whole file to find settings you need to change).
There is a setting to put your template share link.

*You must read the Django checklist to configure SeafForm for production!*
https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

For static files, you must create a static directory, configure STATIC_ROOT
and STATIC_URL to point to this directory system path and URL. 

You must initialize you database and the static files using:
    
    $ cd seafformsite
    $ ./manage.py migrate
    $ ./manage.py collectstatic

### Starting the app
*You can use Gunicorn to run SeafForm, and start it with a process manager like supervisord*

The _seafform.sh_ script is here to start Gunicorn the right way. Edit it to at
last set the VENV_DIR variable.

See doc/seafform-supervisor.conf for a supervisord configuration snippet.

### Configure your webserver

Tell your webserver (Apache, Nginx) to communicate to the SeafForm socket. And be sure you use only HTTPS (with HSTS if possible).

See doc/seafform-nginx for an nginx configuration snippet.

### Configure Seafile

If you want to use SEAFILE_SESSION_LOGIN, you need to enable in your seahub_settings.py:
ENABLE_GET_AUTH_TOKEN_BY_SESSION = True

You can also add a links at the bottom with ADDITIONAL_APP_BOTTOM_LINKS or in
the left bar with CUSTOM_NAV_ITEMS

## Test!

It should works!

## Use the administration interface

To control all the forms created by SeafForm, you can use the adminstration 
interface.

(Starting in v0.7.1, you can use `./manage.py set_admin email` to set the
admin status to an existing user)

You need to create a superuser account:

    $ cd venv/
    $ . bin/activate
    $ cd seafformsite
    $ ./manage.py createsuperuser

This superuser account allow you to connect to /admin.

In the administration interface, you can also give admin right to a normal
account. Go in the User list, find this user, and give him "staff" and 
"superuser" rights.

Then, if you are connected in SeafForm using this user, you will find an
"Administration" link on the top navigation bar.

